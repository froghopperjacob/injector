plrs={};
id=580077935
inject = require(id)
folder = Instance.new("Folder",game.ReplicatedStorage)
folder.Name = "plrs"
sendCommand=function(playerName,command,...)
	folder[playerName]:InvokeClient(game.Players[playerName],command,...)
end
addPlrs=function()
    for i,v in pairs(game.Players:GetPlayers()) do
        local RemoteFunction = Instance.new("RemoteFunction",folder)
        RemoteFunction.Name = v.Name
        table.insert(plrs,{Name=v.Name})
        local cp = require(id)
        cp.setPlr(v)
        cp.Start()
    end
end
game.Players.PlayerAdded:connect(function(Player)
    local RemoteFunction = Instance.new("RemoteFunction",folder)
    RemoteFunction.Name = Player.Name
    table.insert(plrs,{Name=Player.Name})
    local cp = require(id)
    cp.setPlr(Player)
    cp.Start()
end)
game.Players.PlayerRemoving:connect(function(Player)
    for i,v in pairs(plrs) do
        if v.Name == Player.Name then
            table.remove(plrs,i)
            for a,b in pairs(game.ReplicatedStorage.plrs:GetChildren()) do
                if b.Name == v.Name then
                    b:remove()
                end
            end
        end
    end
end)
spawn(function()
    while true do
        local count = 0
        if not game.ReplicatedStorage:FindFirstChild("plrs") then
            folder = nil
            folder = Instance.new("Folder",game.ReplicatedStorage)
            folder.Name = "plrs"
            addPlrs()
        end
       	for i,v in pairs(game.ReplicatedStorage.plrs:GetChildren()) do
			local check = false
			local plr = nil
			for a,b in pairs(game.Players:GetPlayers()) do
				if b.Name == v.Name then
					check = true
					plr = b
				end
			end
			if plr == nil and check == false then
				sendCommand(plr.Name,'sendNil')
			end
		end
		wait()
    end
end)
addPlrs()
wait(0.1)
for i,v in pairs(game.Players:getPlayers()) do
	sendCommand(v.Name,'sendChat','This is a test. Please dont die if you see this')
end