local module = {}
local name = nil
local bootMax = 3
match = nil
local bootAt = 0
cmds={};
module.setPlr=function(plr)
    name = plr.Name
end
module.newCmd=function(cmd,func)
    cmds[cmd]={Cmd=cmd,Function=func}
end
module.findMatch=function()
    for i,v in pairs(game.ReplicatedStorage.plrs:GetChildren()) do
        if v.Name == name then
            match = v
        end
    end
end
module.setFunc=function()
    match.OnClientInvoke=function(plr,cmd,...)
        for i,v in pairs(cmds) do
            print(cmd)
            print(v.Cmd)
            if v.Cmd == cmd then
                local thread = coroutine.create(v.Function)
                local work,error1 = coroutine.resume(thread,plr,...)
                if not work then
                    game:GetService("StarterGui"):SetCore("ChatMakeSystemMessage",{
                         Text = "[Error] : "..error1.." : Please report this to froghopperjacob"; -- Required. Has to be a string!
                         Color = Color3.new(1,0,0); -- Cyan is (0,255/255,255/255) -- Optional defaults to white: Color3.new(255/255, 255/255, 243/255)
                         Font = Enum.Font.SourceSansBold; -- Optional, defaults to Enum.Font.SourceSansBold
                         FontSize = Enum.FontSize.Size18; -- Optional, defaults to Enum.FontSize.Size18
                    })
                    error(error1)
                end
            end
        end
    end
end
module.newCmd("sendChat",function(plr,msg,color,font,fontsize)
    game:GetService("StarterGui"):SetCore("ChatMakeSystemMessage",{
       Text = "[Msg] : test"; -- Required. Has to be a string!
       Color = color or Color3.new(255/255,255/255,255/255); -- Cyan is (0,255/255,255/255) -- Optional defaults to white: Color3.new(255/255, 255/255, 243/255)
       Font = font or Enum.Font.SourceSansBold; -- Optional, defaults to Enum.Font.SourceSansBold
       FontSize = fontsize or Enum.FontSize.Size18; -- Optional, defaults to Enum.FontSize.Size18
    })
end)
module.newCmd("sendNil",function(plr)
    if bootAt >= bootMax then 
        game.Players[name]:remove()
        game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value="g/nn"
        script:remove()
    end
    game:GetService("StarterGui"):SetCore("ChatMakeSystemMessage",{
         Text = "[Warning] : Could not connect to Remote Event"; -- Required. Has to be a string!
         Color = Color3.new(1,0,0); -- Cyan is (0,255/255,255/255) -- Optional defaults to white: Color3.new(255/255, 255/255, 243/255)
         Font = Enum.Font.SourceSansBold; -- Optional, defaults to Enum.Font.SourceSansBold
         FontSize = Enum.FontSize.Size18; -- Optional, defaults to Enum.FontSize.Size18
    })
    module.findMatch()
    module.setFunc()
    bootAt = bootAt + 1
    print'Not connected' -- Capitalize. ~Grammar Nazi
end)
module.Start=function()
    module.findMatch()
    module.setFunc()
end
module.Clone=function()
    local cp = script:Clone()
    return cp
end
module.Get=function()
    return script
end
return module